package com.eduardorabanal.app.retrofitpractice.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 22/11/2017.
 */
public class ServiceProveedor {
    public static ServiceContacto getService()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.0.7/Foro/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(ServiceContacto.class);
    }
}
