package com.eduardorabanal.app.retrofitpractice.models;

/**
 * Created by USER on 22/11/2017.
 */
public class Contacto {
    private int ContactoId;
    private String Nombre;
    private String Telefono;
    private String Email;

    public Contacto() {
    }

    public Contacto(int contactoId, String nombre, String telefono, String email) {
        ContactoId = contactoId;
        Nombre = nombre;
        Telefono = telefono;
        Email = email;
    }

    public int getContactoId() {
        return ContactoId;
    }

    public void setContactoId(int contactoId) {
        ContactoId = contactoId;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
