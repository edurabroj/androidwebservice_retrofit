package com.eduardorabanal.app.retrofitpractice.service;

import com.eduardorabanal.app.retrofitpractice.models.Contacto;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by USER on 22/11/2017.
 */
public interface ServiceContacto {
    @GET("Contacto")
    Call<List<Contacto>> ListaContactos();

    @FormUrlEncoded
    @POST("Contacto/Create")
    Call<ResponseBody> CrearContacto(@Field("Nombre") String nombre,
                                     @Field("Telefono") String telefono,
                                     @Field("Email") String email);
}
