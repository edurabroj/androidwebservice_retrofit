package com.eduardorabanal.app.retrofitpractice.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eduardorabanal.app.retrofitpractice.R;
import com.eduardorabanal.app.retrofitpractice.service.ServiceContacto;
import com.eduardorabanal.app.retrofitpractice.service.ServiceProveedor;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateActivity extends AppCompatActivity {
    ServiceContacto serviceContacto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        serviceContacto = ServiceProveedor.getService();

        Button btnSave = (Button) findViewById(R.id.btnSave);
        final EditText etNombre = (EditText) findViewById(R.id.etNombre);
        final EditText etTelefono = (EditText) findViewById(R.id.etTelefono);
        final EditText etEmail = (EditText) findViewById(R.id.etEmail);

        assert btnSave != null;
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceContacto.CrearContacto(etNombre.getText().toString(),
                                              etTelefono.getText().toString(),
                                              etEmail.getText().toString()).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(getBaseContext(),"guardado bien",Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(CreateActivity.this,MainActivity.class));
                        }else{
                            Toast.makeText(getBaseContext(),"bad request",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getBaseContext(),"error",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
