package com.eduardorabanal.app.retrofitpractice.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.eduardorabanal.app.retrofitpractice.R;
import com.eduardorabanal.app.retrofitpractice.models.Contacto;

import java.util.List;

/**
 * Created by USER on 22/11/2017.
 */
public class AdapterLv extends BaseAdapter {
    private Context context;
    private List<Contacto> dataset;

    public AdapterLv(Context context, List<Contacto> dataset) {
        this.context = context;
        this.dataset = dataset;
    }

    public void setDataset(List<Contacto> dataset) {
        this.dataset = dataset;
    }

    @Override
    public int getCount() {
        return dataset.size();
    }

    @Override
    public Object getItem(int position) {
        return dataset.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_lv, parent, false);
        }

        final Contacto contacto = (Contacto) getItem(position);

        final TextView tvNombre = (TextView) convertView.findViewById(R.id.tvNombre);
        tvNombre.setText(contacto.getNombre());

        final TextView tvTelefono = (TextView) convertView.findViewById(R.id.tvTelefono);
        tvTelefono.setText(contacto.getTelefono());

        return convertView;
    }
}
