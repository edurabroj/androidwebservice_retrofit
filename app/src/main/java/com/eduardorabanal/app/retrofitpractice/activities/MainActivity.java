package com.eduardorabanal.app.retrofitpractice.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.eduardorabanal.app.retrofitpractice.R;
import com.eduardorabanal.app.retrofitpractice.adapters.AdapterLv;
import com.eduardorabanal.app.retrofitpractice.models.Contacto;
import com.eduardorabanal.app.retrofitpractice.service.ServiceContacto;
import com.eduardorabanal.app.retrofitpractice.service.ServiceProveedor;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private ServiceContacto serviceContacto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnCreate = (Button) findViewById(R.id.btnCreate);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,CreateActivity.class));
            }
        });

        ListView lv = (ListView) findViewById(R.id.lv);
        final AdapterLv adapter = new AdapterLv(this, new ArrayList<Contacto>());
        lv.setAdapter(adapter);

        serviceContacto = ServiceProveedor.getService();

        serviceContacto.ListaContactos().enqueue(new Callback<List<Contacto>>() {
            @Override
            public void onResponse(Call<List<Contacto>> call, Response<List<Contacto>> response) {
                if(response.isSuccessful()){
                    List<Contacto> lista = response.body();
                    for(Contacto c: lista){
                        Log.i("DATOS", c.getNombre());
                    }
                    adapter.setDataset(lista);
                }else{
                    Log.i("DATOS", "RESPUESTA INSATISFACTORIA");
                }
            }

            @Override
            public void onFailure(Call<List<Contacto>> call, Throwable t) {
                Log.i("DATOS", "RESPUESTA FALLIDA" + t.getMessage());
            }
        });
    }
}
